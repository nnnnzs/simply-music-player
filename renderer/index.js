const { ipcRenderer } = require('electron')
const { $, convertDuration } = require('./helper');
let musicAudio = new Audio();
let allTracks;
let currentTrack;

$('add-music-button').addEventListener('click', () => {
    ipcRenderer.send('add-music-window')
})
const renderListHTML = (tracks) => {
    const tracksList = $('tracksList');
    if(!Array.isArray(tracks)){
        return
    }
    const tracksListHTML = tracks.reduce((html, track) => {
        html += `
        <li class=" row music-track list-group-item d-flex justify-content-between align-items-center">
        <div class="col-10">
            <i class="fas fa-music mr-2 text-secondary"></i>
            <b>${track.fileName}</b>
        </div>
        <div class="col-2">
            <i class="fas fa-play mr-2" data-id="${track.id}"></i>
            <i class="fas fa-trash-alt mr-2" data-id="${track.id}"></i>
        </div>
        </li>`
        return html;
    }, '');
    const emptyTrackHTML = `<div class="alert alert-primary">还没有添加任何音乐</div>`
    tracksList.innerHTML = tracks.length ? `<ul class="list-group">${tracksListHTML}</ul>` : emptyTrackHTML
}
ipcRenderer.on('getTracks', (event, tracks) => {
    allTracks = tracks;
    renderListHTML(tracks)
})
const renderPlayerHTML = (name, duration) => {
    const palyer = $('player-status')
    const html = `<div class="col font-weight-bold">
                    正在播放${name}
                </div>
                <div class="col">
                    <span id="current-seeker">00:00</span> / ${convertDuration(duration)}
                </div>`
    palyer.innerHTML = html;
}
const updateProgressHTML = (currentTime, duration) => {
    //计算progress 
    const progress = Math.floor(currentTime / duration * 100)
    const bar = $('player-progress')
    const seeker = $('current-seeker');
    bar.innerHTML = progress + '%';
    bar.style.width = progress + '%';
    seeker.innerHTML = convertDuration(currentTime);
}
musicAudio.addEventListener('loadedmetadata', () => {
    //渲染播放器状态
    renderPlayerHTML(currentTrack.fileName, musicAudio.duration)
})
musicAudio.addEventListener('timeupdate', () => {
    //更新播放器状态
    updateProgressHTML(musicAudio.currentTime, musicAudio.duration);
})

$('tracksList').addEventListener('click', e => {
    //事件捕获，代理到列表
    e.preventDefault();
    const { dataset, classList } = event.target;
    const id = dataset && dataset.id;
    if (id && classList.contains('fa-play')) {
        //开始播放音乐
        // 如果是暂停，且是当前的歌曲，还原播放进度
        if (currentTrack && currentTrack.id === id) {
            //继续播放
            musicAudio.play();
        } else {
            //播放新的
            currentTrack = allTracks.find(track => track.id === id)
            musicAudio.src = currentTrack.path;
            musicAudio.play();
            const resetIconEle = document.querySelector('.fa-pause');
            if (resetIconEle) {
                resetIconEle.classList.replace('fa-pause', 'fa-play')
            }
        }
        classList.replace('fa-play', 'fa-pause')
    } else if (id && classList.contains('fa-pause')) {
        //暂停
        musicAudio.pause();
        classList.replace('fa-pause', 'fa-play')
    } else if (id && classList.contains('fa-trash-alt')) {
        //删除
        ipcRenderer.send('delete-track', id)
    }
})