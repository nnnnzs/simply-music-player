const { app, BrowserWindow, ipcMain, dialog, Tray, Menu } = require('electron');
const DataStore = require('./MusicDataStore');
const myStore = new DataStore({ name: 'MusicData' })

class AppWindow extends BrowserWindow {
    constructor(config, fileLocation) {
        const basicConfig = {
            width: 800,
            height: 600,
            showDevTools: true,
            webPreferences: {
                nodeIntegration: true,
            }
        }
        const finalConfig = { ...basicConfig, ...config }
        super(finalConfig)
        this.loadFile(fileLocation)
        this.once('ready-to-show', () => {
            this.show();
        })
    }
}

app.on('ready', () => {
    const mainWindow = new AppWindow({}, './renderer/index.html')
    // mainWindow.webContents.openDevTools()
    mainWindow.webContents.on('did-finish-load',()=>{
        mainWindow.send('getTracks',myStore.getTracks())
    })
    //打开添加音乐的窗口
    ipcMain.on('add-music-window', () => {
        const addWindow = new AppWindow({
            width: 500,
            height: 400,
            parent: mainWindow,
        }, './renderer/add.html');
    })
    //添加音乐
    ipcMain.on('add-tracks', (e, tracks) => {
        const updatedTracks = myStore.addTracks(tracks).getTracks();
        mainWindow.send('getTracks', updatedTracks)
    })
    ipcMain.on('delete-track',(event,id)=>{
        const updatedTracks = myStore.deleteTrack(id)
        mainWindow.send('getTracks',updatedTracks)
    })
    //打开选择文件的框
    ipcMain.on('open-music-file', (event) => {
        dialog.showOpenDialog({
            title: '选择音乐',
            properties: ['openFile', 'multiSelections'],
            filters: [{ name: 'Music', extensions: ['mp3'] }]
        }).then(files => {
            const { canceled, filePaths } = files;
            event.sender.send('selected-file', filePaths)
        })
    })
    // let tray = new Tray('static/img/u449.png')
    // const contextMenu = Menu.buildFromTemplate([
    //     { label: 'Item1', type: 'radio' },
    //     { label: 'Item2', type: 'radio' },
    //     { label: 'Item3', type: 'radio', checked: true },
    //     { label: 'Item4', type: 'radio' }
    // ])
    // tray.setToolTip('This is my application.')
    // tray.setContextMenu(contextMenu)

    // //闪标
    // setInterval(() => {
    //     mainWindow.flashFrame(false)
    //     mainWindow.flashFrame(true)
    // }, 1000)
})
